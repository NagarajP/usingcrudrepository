package com.myzee.lombok.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.myzee.lombok.entity.Department;
import com.myzee.lombok.service.DepartmentService;

@RestController
public class DepartmentController {
	
	@Autowired
	DepartmentService departmentService;
	
	@PostMapping(value = "/add/department", produces = "application/json")
	private void saveDepartment(@RequestBody Department dept) {
		// TODO Auto-generated method stub
		departmentService.saveDepartment(dept);
	}
	
	@GetMapping("/get/departments")
	private  List<Department> getDepartments() {
		return departmentService.fetchDepartmentList();
	}

	@PutMapping(value = "/update/department/{id}", produces = "application/json")
	private Department updateDepartment(@RequestBody Department department, 
			@PathVariable("id") Long departmentId) {
		return departmentService.updateDepartment(department, departmentId);
	}
	
	@DeleteMapping(value = "/delete/department/{id}", consumes = "application/html")
	private String deleteDepartment(@PathVariable("id") Long departmentId) {
		departmentService.deleteDepartmentById(departmentId);
		return "Department delete succefully!!";
	}
}
