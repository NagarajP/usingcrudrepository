package com.myzee.lombok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsingCrudrepository {

	public static void main(String[] args) {
		SpringApplication.run(UsingCrudrepository.class, args);
	}

}
