package com.myzee.lombok.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.myzee.lombok.entity.Department;
import com.myzee.lombok.repository.DepartmentRepository;

public interface DepartmentService {

	Department saveDepartment(Department d);
	List<Department> fetchDepartmentList();
	Department updateDepartment(Department d, Long departmentId);
	void deleteDepartmentById(Long departmentId);
}
