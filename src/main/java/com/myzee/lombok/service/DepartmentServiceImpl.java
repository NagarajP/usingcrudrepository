package com.myzee.lombok.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myzee.lombok.entity.Department;
import com.myzee.lombok.repository.DepartmentRepository;

@Service
public class DepartmentServiceImpl implements DepartmentService {
	
	@Autowired
	DepartmentRepository repository;

	@Override
	public Department saveDepartment(Department d) {
		return repository.save(d);
	}

	@Override
	public List<Department> fetchDepartmentList() {
		return (List<Department>) repository.findAll();
	}

	@Override
	public Department updateDepartment(Department department, Long departmentId) {
		
		Department depDB
        = (Department) repository.findById(departmentId)
              .get();

    if (Objects.nonNull(department.getDepartmentName())
        && !"".equalsIgnoreCase(
            department.getDepartmentName())) {
        depDB.setDepartmentName(
            department.getDepartmentName());
    }

    if (Objects.nonNull(
            department.getDepartmentAddress())
        && !"".equalsIgnoreCase(
            department.getDepartmentAddress())) {
        depDB.setDepartmentAddress(
            department.getDepartmentAddress());
    }

    if (Objects.nonNull(department.getDepartmentCode())
        && !"".equalsIgnoreCase(
            department.getDepartmentCode())) {
        depDB.setDepartmentCode(
            department.getDepartmentCode());
    }

    return repository.save(depDB);
	}

	@Override
	public void deleteDepartmentById(Long departmentId) {
		repository.deleteById(departmentId);
	}

}
