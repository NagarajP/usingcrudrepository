package com.myzee.lombok.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.myzee.lombok.entity.Department;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {

//	@SuppressWarnings("unchecked")
//	Department save(Department d);
//	List<Department> findAll();

}
